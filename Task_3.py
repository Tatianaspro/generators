# Задание 2
# Перепишите решение первого задания с помощью генератора.

def my_reverse(iterable):
    i = len(iterable) - 1

    while i >= 0:
        yield iterable[i]
        i -= 1


def main():
    iterables = [1, 2, 3, 4, 5, 6, 7]
    for i in my_reverse(iterables):
        print(i)


if __name__ == '__main__':
    main()
