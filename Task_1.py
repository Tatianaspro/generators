# Задание 1
# Напишите итератор, который возвращает элементы заданного списка в обратном порядке (аналог
# reversed).

class my_reverse(object):

    #  __slots__ = ('__dict__','iterable')
    def __init__(self, iterable):
        self.iterable = iterable
        self.i = len(iterable) - 1

    def __iter__(self):
        return self

    def __next__(self):
        if self.i < 0:
            raise StopIteration

        reverse = self.iterable[self.i]
        self.i -= 1
        return reverse


def main():
    iterables = [1, 2, 3, 4, 5, 6]
    for i in my_reverse(iterables):
        print(i)


if __name__ == '__main__':
    main()
